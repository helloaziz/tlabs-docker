# Dokumentasi Proyek Swarm Microservice Demo - DevOps Engineer - Tlabs

Selamat datang di dokumentasi proyek Swarm Microservice Demo! Dalam proyek ini, saya mencoba untuk mendeploy layanan microservice menggunakan Kubernetes di lingkungan lokal (WSL 2). Saya akan memandu Anda melalui langkah-langkah yang saya ambil, hambatan yang saya temui, dan hasil akhir proyek ini.

## Langkah-langkah Instalasi dan Setup

### 1. Instalasi Minikube dan Docker di WSL 2
Pertama-tama, saya menginstal Minikube untuk mengelola kluster Kubernetes lokal di WSL 2. Saya juga memastikan Docker sudah terinstal dan terintegrasi dengan WSL.

```bash
# Instalasi Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb

# Instalasi Docker (jika belum terinstal)
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
### 2. Clone Repositori Microservice Demo
Saya mengunduh repositori Microservice Demo dari Docker dan memodifikasi file deployment.yaml agar sesuai dengan lingkungan Kubernetes.
```bash
git clone https://github.com/docker-archive/swarm-microservice-demo-v1.git
cd swarm-microservice-demo-v1
```
### 3. Setup Kubernetes dan Deploy Service
Selanjutnya, saya menggunakan Minikube untuk memulai kluster Kubernetes dan melakukan deployment service Microservice Demo.
```bash
minikube start --nodes 3
kubectl apply -f deployment.yaml
```
### 4. Setup CI/CD dengan GitLab
Saya membuat file .gitlab-ci.yml untuk melakukan CI/CD menggunakan GitLab. Namun, saya mengalami kendala saat runner tidak dapat terhubung ke daemon Docker.

### Kendala yang Ditemui
Saat melakukan deployment, saya menghadapi beberapa kendala seperti:

#### NodePort vs. LoadBalancer: 

LoadBalancer tidak dapat bekerja dengan baik di lingkungan lokal WSL 2, jadi saya beralih ke NodePort untuk mengakses service.

#### Masalah dengan GitLab Runner: 

Runner tidak dapat terhubung ke daemon Docker, yang menyebabkan kegagalan proses CI/CD.

### Bonus Poin yang Tidak Dikerjakan
Meskipun saya berusaha untuk mencapai bonus poin, ada beberapa hal yang belum saya implementasikan:

Setup VM/VPS dengan Terraform atau Ansible.

Penggunaan Rancher untuk manajemen Kubernetes.

Setup logging service dengan ELK atau Grafana Loki.

Setup monitoring & alerting dengan Grafana & Prometheus.


## Kesimpulan
Meskipun proyek ini belum sepenuhnya mencapai semua tujuan yang diinginkan, saya telah berhasil mendeploy service Microservice Demo menggunakan Kubernetes di lingkungan lokal. Kendala dengan GitLab Runner dihost tidak dapat diatasi, namun, saya senang dengan progres dan pengalaman yang saya peroleh.

Meski terdapat beberapa kendala teknis dan bonus poin yang tidak tercapai, proyek ini memberikan pengalaman berharga dalam mengelola layanan dan alur kerja CI/CD menggunakan GitLab dan Kubernetes di lingkungan lokal.

Terima kasih telah mengikuti dokumentasi ini! Jika ada pertanyaan atau saran lebih lanjut, jangan ragu untuk menghubungi saya.
